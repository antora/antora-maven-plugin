#!/bin/bash

export PAGER=cat

./mvnw spotless:apply
rm -rf file%3A*

./mvnw -q dependency:copy -Dartifact=org.eclipse.jgit:org.eclipse.jgit.pgm:LATEST:sh -DoutputDirectory=target -Dmdep.stripVersion -Dsilent

chmod 755 target/org.eclipse.jgit.pgm.sh

if [ -n "$(./target/org.eclipse.jgit.pgm.sh diff --name-status | grep ^M)" ]; then
  echo The following pending formatting changes were detected. Halting build.
  ./target/org.eclipse.jgit.pgm.sh diff --name-status | grep ^M | cut -f2 | xargs ./target/org.eclipse.jgit.pgm.sh diff --
  exit 1
fi

exit 0
