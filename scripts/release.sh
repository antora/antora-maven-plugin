#!/bin/bash

if [ -z "$RELEASE_DEPLOY_KEY" ]; then
  declare -n RELEASE_DEPLOY_KEY="RELEASE_DEPLOY_KEY_$GITLAB_USER_LOGIN"
  if [ -z "$RELEASE_DEPLOY_KEY" ]; then
    echo No release deploy key \(RELEASE_DEPLOY_KEY or RELEASE_DEPLOY_KEY_$GITLAB_USER_LOGIN\) defined. Halting release.
    exit 1
  fi
fi
if [ -z "$RELEASE_GPG_PASSPHRASE" ]; then
  declare -n RELEASE_GPG_PASSPHRASE_REF="RELEASE_GPG_PASSPHRASE_$GITLAB_USER_LOGIN"
  export RELEASE_GPG_PASSPHRASE="$(echo -n $RELEASE_GPG_PASSPHRASE_REF)"
  if [ -z "$RELEASE_GPG_PASSPHRASE" ]; then
    echo No release gpg passphrase \(RELEASE_GPG_PASSPHRASE or RELEASE_GPG_PASSPHRASE_$GITLAB_USER_LOGIN\) defined. Halting release.
    exit 1
  fi
fi
if [ -z "$RELEASE_GPG_PRIVATE_KEY" ]; then
  declare -n RELEASE_GPG_PRIVATE_KEY="RELEASE_GPG_PRIVATE_KEY_$GITLAB_USER_LOGIN"
  if [ -z "$RELEASE_GPG_PRIVATE_KEY" ]; then
    echo No release gpg private key \(RELEASE_GPG_PRIVATE_KEY or RELEASE_GPG_PRIVATE_KEY_$GITLAB_USER_LOGIN\) defined. Halting release.
    exit 1
  fi
fi
# RELEASE_VERSION must be an exact version number
if [ -z "$RELEASE_VERSION" ]; then
  echo No release version \(RELEASE_VERSION\) defined. Halting release.
  exit 1
fi
if [ "$RELEASE_VERSION" == "${RELEASE_VERSION%-*}" ]; then
  export NEXT_VERSION="${RELEASE_VERSION%.*}.$((${RELEASE_VERSION##*.} + 1))-SNAPSHOT"
else
  export NEXT_VERSION="${RELEASE_VERSION%-*}-SNAPSHOT"
fi
export RELEASE_DATE="$(date +%Y-%m-%d)"
export RELEASE_BRANCH=${CI_COMMIT_BRANCH:-main}
RELEASE_ARTIFACT_ID=$(./mvnw -Dexpression=project.artifactId -DforceStdout=true -q help:evaluate)
RELEASE_TAG=$RELEASE_ARTIFACT_ID-$RELEASE_VERSION

# set up GPG
echo -n "$RELEASE_GPG_PRIVATE_KEY" | gpg --import --batch

# set up SSH auth using ssh-agent
mkdir -p -m 700 $HOME/.ssh
ssh-keygen -F gitlab.com >/dev/null 2>&1 || ssh-keyscan -H -t rsa gitlab.com >> $HOME/.ssh/known_hosts 2>/dev/null
eval $(ssh-agent -s) >/dev/null
if [ -f "$RELEASE_DEPLOY_KEY" ]; then
  chmod 600 $RELEASE_DEPLOY_KEY
  ssh-add -q $RELEASE_DEPLOY_KEY
else
  echo -n "$RELEASE_DEPLOY_KEY" | ssh-add -q -
fi
exit_code=$?
if [ $exit_code -gt 0 ]; then
  exit $exit_code
fi
echo Deploy key identity added to SSH agent.

# configure git to push changes
git config --local user.name "$GITLAB_USER_NAME"
git config --local user.email "$GITLAB_USER_EMAIL"
git remote set-url origin git@gitlab.com:$CI_PROJECT_PATH.git
git fetch --depth ${GIT_DEPTH:-5} --update-shallow origin $RELEASE_BRANCH

# make sure the release branch exists as a local branch
git checkout -b $RELEASE_BRANCH -t origin/$RELEASE_BRANCH

if [ "$(git rev-parse $RELEASE_BRANCH)" != "$CI_COMMIT_SHA" ]; then
  echo $RELEASE_BRANCH moved forward from $CI_COMMIT_SHA. Halting release.
  exit 1
fi

# release!
(
  set -e
  ./scripts/version.sh
  git stash -q
  ./mvnw $MAVEN_CLI_OPTS -Pprepare-release -DreleaseVersion=$RELEASE_VERSION -DdevelopmentVersion=$NEXT_VERSION -DpushChanges=false release:prepare
  git tag $RELEASE_TAG $RELEASE_TAG^{} -f -m "tag $RELEASE_TAG"
  ./scripts/postrelease.sh
  git commit -a --amend --no-edit
  git push origin $RELEASE_TAG
  sed -i '/^exec.activateProfiles=/d' release.properties
  ./mvnw $MAVEN_CLI_OPTS -Ppublish-release -s $RELEASE_MAVEN_SETTINGS release:perform
  git push origin $RELEASE_BRANCH
)
exit_code=$?

# report any uncommitted files
git status -s -b

# kill the ssh-agent
eval $(ssh-agent -k) >/dev/null

exit $exit_code
