#!/bin/bash

shopt -s extglob

if [ "$RELEASE_VERSION" == "${RELEASE_VERSION%-*}" ]; then
  sed -i "s/^\(prerelease:\) .*/\1 false/" docs/antora.yml
else
  sed -i "s/^\(prerelease:\) .*/\1 true/" docs/antora.yml
fi
sed -i "s/^\(version:\) .*/\1 '${NEXT_VERSION%.*-*}'/" docs/antora.yml
sed -i "s/^\(:revnumber:\) .*/\1 $NEXT_VERSION/" README.adoc
