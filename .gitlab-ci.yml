# see https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/lib/gitlab/ci/templates/Maven.gitlab-ci.yml for reference
workflow:
  rules:
  - if: $CI_PIPELINE_SOURCE == 'merge_request_event' || $CI_PIPELINE_SOURCE == 'schedule' || $CI_PIPELINE_SOURCE == 'web'
  - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS == null && $CI_COMMIT_BRANCH !~ /^docs\//
    changes:
    - .gitlab-ci.yml
    - .mvn/wrapper/*
    - pom.xml
    - src/**/*
variables:
  GIT_DEPTH: '5'
  # not using -DinstallAtEnd=true and -DdeployAtEnd=true since they aren't relevant for this single-module project
  MAVEN_OPTS: '-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.showDateTime=true -Dorg.slf4j.simpleLogger.dateTimeFormat=HH:mm:ss -Djava.awt.headless=true'
  # enable -ntp once we're confident the cache is working properly
  MAVEN_CLI_OPTS: '--batch-mode --errors --fail-at-end --show-version'
  RELEASE_VERSION:
    value: '~'
    description: The version to release from the selected protected branch if the build succeeds. To enable, specify an exact version.
  RELEASE_GPG_PRIVATE_KEY:
    value: '~'
    description: The GPG private key for signing release artifacts.
  ENABLE_COMPAT_JOB:
    value: '~'
    description: Enter 1 or true to enable the test:compat job.
  ENABLE_WINDOWS_JOB:
    value: '~'
    description: Enter 1 or true to enable the test:windows job.
default:
  image: eclipse-temurin:17
  interruptible: true
.defs:
- &if_docs_mr
  rules:
  - if: &docs_mr $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^docs\//
- &unless_docs_mr
  rules:
  - if: *docs_mr
    when: never
  - if: $CI_JOB_NAME == 'build' && $CI_PIPELINE_SOURCE == 'push' && $CI_PROJECT_PATH != 'antora/antora-maven-plugin'
    when: manual
  - when: on_success
- &if_release
  rules:
  - if: $CI_PIPELINE_SOURCE == 'web' && $CI_PROJECT_PATH == 'antora/antora-maven-plugin' && $CI_COMMIT_BRANCH && $CI_COMMIT_REF_PROTECTED && $RELEASE_VERSION && $RELEASE_VERSION != '~'
- &if_compat
  rules:
  - if: $CI_PIPELINE_SOURCE == 'schedule' || ($CI_PIPELINE_SOURCE == 'web' && $CI_JOB_NAME =~ /^test:compat-/ && $ENABLE_COMPAT_JOB && $ENABLE_COMPAT_JOB != '~')
- &if_windows
  rules:
  - if: $CI_PIPELINE_SOURCE == 'schedule' || ($CI_PIPELINE_SOURCE == 'web' && $CI_JOB_NAME == 'test:windows' && $ENABLE_WINDOWS_JOB && $ENABLE_WINDOWS_JOB != '~')
- &save_report_artifacts
  artifacts:
    when: always
    paths:
    - target/invoker-reports/TEST-*.xml
    reports:
      junit: target/invoker-reports/TEST-*.xml
.maven:
  stage: test
  <<: *unless_docs_mr
  cache: &maven_cache
    key: $CI_COMMIT_REF_SLUG
    policy: pull
    paths:
    - .m2/repository
build:
  extends: .maven
  stage: build
  cache:
    <<: *maven_cache
    policy: pull-push
  script:
  - ./mvnw $MAVEN_CLI_OPTS -Dmaven.test.skip=true verify
  - ./scripts/detect-pending-formatting.sh
test:linux:
  extends: .maven
  script:
  - ./mvnw $MAVEN_CLI_OPTS -Dmaven.javadoc.skip=true -Dinvoker.streamLogsOnFailures=true -Dinvoker.writeJunitReport=true verify
  <<: *save_report_artifacts
test:compat-min:
  extends: .maven
  <<: *if_compat
  before_script:
  - ./mvnw wrapper:wrapper -Dtype=only-script -Dmaven=$(./mvnw -Dexpression=project.prerequisites.maven -DforceStdout=true -q help:evaluate)
  script:
  - ./mvnw $MAVEN_CLI_OPTS -Dmaven.javadoc.skip=true -Dinvoker.streamLogsOnFailures=true verify
test:compat-next:
  extends: .maven
  <<: *if_compat
  # remove MAVEN_OPTS override after upgrading to 4.0.0-rc-3
  variables:
    MAVEN_OPTS: '-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true'
  before_script:
  - ./mvnw wrapper:wrapper -Dtype=only-script -Dmaven=4.0.0-rc-2
  script:
  - ./mvnw $MAVEN_CLI_OPTS -Dmaven.javadoc.skip=true -Dinvoker.streamLogsOnFailures=true verify
test:windows:
  extends: .maven
  <<: *if_windows
  tags: [saas-windows-medium-amd64]
  cache: [] # cache does not currently work with the shared Windows runner, so don't waste time trying
  before_script:
  - choco install -y temurin17
  script:
  - $env:JAVA_HOME="$(node -p "((d) => d + require('fs').readdirSync(d)[0])('C:\\Program Files\\Eclipse Adoptium\\')")"
  - .\mvnw (Get-ChildItem Env:MAVEN_CLI_OPTS).Value.Split(" ") "-Dmaven.javadoc.skip=true" "-Dinvoker.streamLogsOnFailures=true" verify
release:
  <<: *if_release
  stage: deploy
  cache:
    <<: *maven_cache
    policy: pull-push
  environment: releases
  interruptible: false
  before_script:
  - apt-get -qq update
  - apt-get install -y git gpg openssh-client > /dev/null
  script:
  - ./scripts/release.sh
