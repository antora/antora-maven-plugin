= Antora Maven Plugin
:revnumber: 1.0.0-SNAPSHOT
:release-version: 1.0.0-alpha.5
:icons: font

This project provides the Antora Maven plugin that installs and runs https://antora.org[Antora] using a managed Node.js runtime.

The Antora Maven plugin adds support for using Antora from a Maven-based project by adding a goal named `antora:antora` that installs and invokes Antora as well as a lifecycle phase named `antora` that invokes this goal.
You do not need Node.js preinstalled to use this plugin.

Refer to the https://docs.antora.org/maven-plugin/latest/[documentation] to learn how to install, configure, and use this plugin.

TIP: Check out the https://docs.antora.org/gradle-plugin/latest/[Gradle Antora Plugin] to find a similar plugin for Gradle-based projects.

== Copyright and License

Copyright (C) 2023-present Rob Winch, OpenDevise Inc., and the original author(s) of the project files.

This project is open source software.
Use of this software is granted under the terms of the https://www.mozilla.org/en-US/MPL/2.0/[Mozilla Public License Version 2.0] (MPL-2.0).
See link:LICENSE[] to find the full license text.

== Authors

The Antora Maven Plugin is co-led by Rob Winch and OpenDevise Inc.
The project was initiated in 2023 by Rob Winch, then later adopted by the Antora project in the same year.
