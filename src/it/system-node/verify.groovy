File nodeDir = new File(basedir, "target/node")
assert nodeDir.isDirectory()
File nodeModulesDir = new File(basedir, "node_modules")
assert !nodeModulesDir.exists()
File siteIndexFile = new File(basedir, "target/site/test/1.0/index.html")
assert siteIndexFile.isFile()
String stdout = new File(basedir, "build.log").text
assert stdout.contains("Installed node locally.")
assert stdout.contains("Using com.github.eirslett:frontend-maven-plugin:1.14.2 to manage Node.js")
assert stdout.contains("ing system Node.js for nodeExecutable")
File nodeExec = new File(nodeDir, 'node')
String nodeVersion = "${nodeExec} --version".execute().text.trim()
assert nodeVersion.equals("v18.19.1")
