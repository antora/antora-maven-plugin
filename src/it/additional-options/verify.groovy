File nodeDir = new File(basedir, "target/node")
assert nodeDir.isDirectory()
File siteIndexFile = new File(basedir, "target/site/test/1.0/index.html")
assert siteIndexFile.isFile()
String stdout = new File(basedir, "build.log").text
assert stdout.contains("skipping reference to missing attribute: product-name")
assert stdout.contains("Running 'npx --yes --package antora antora --clean --fetch --log-failure-level fatal antora-playbook.yml'")
