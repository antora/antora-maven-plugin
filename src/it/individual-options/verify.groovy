File nodeDir = new File(basedir, "target/node")
assert nodeDir.isDirectory()
File siteIndexFile = new File(basedir, "target/site/test/1.0/index.html")
assert siteIndexFile.isFile()
String indexText = siteIndexFile.text
assert indexText.contains("ACME")
String stdout = new File(basedir, "build.log").text
assert stdout.contains(" --fetch ")
assert stdout.contains(" --start-page test::index.adoc ")
assert stdout.contains(" --attribute product-name=ACME --attribute foo=baz ")
