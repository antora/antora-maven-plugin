File nodeDir = new File(basedir, "target/node")
assert nodeDir.isDirectory()
File nodeModulesDir = new File(basedir, "node_modules")
assert nodeModulesDir.isDirectory()
File packageLockFile = new File(basedir, "package-lock.json")
assert !packageLockFile.exists()
File siteIndexFile = new File(basedir, "target/site/test/1.0/index.html")
assert siteIndexFile.isFile()
String stdout = new File(basedir, "build.log").text
assert stdout.contains("Running 'npm i'")
assert !stdout.contains("audited")
assert stdout.contains("Running 'npx --offline antora")
