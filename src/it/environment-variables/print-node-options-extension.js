'use strict'

module.exports.register = function () {
  this.once('contextStarted', () => {
    console.log('NODE_OPTIONS=' + process.env.NODE_OPTIONS)
    console.log('IS_TTY=' + process.env.IS_TTY)
    if ('FORCE_COLOR' in process.env) console.log('FORCE_COLOR=true')
  })
}
