String stdout = new File(basedir, "build.log").text
assert stdout.contains("A Maven plugin that runs Antora on your project.")
assert stdout.contains("This plugin has 2 goals:")
assert stdout.contains("playbookFile (Default: antora-playbook.yml)")
