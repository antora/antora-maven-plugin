File nodeDir = new File(basedir, "target/node")
assert nodeDir.isDirectory()
File siteIndexFile = new File(basedir, "build/site/test/1.0/index.html")
assert siteIndexFile.isFile()
String indexText = siteIndexFile.text
assert indexText.contains("My Site")
assert indexText.contains("The \"GOAT\" Service")
assert indexText.contains("https://docs.example.org")
String stdout = new File(basedir, "build.log").text
assert stdout.contains("antora --clean --stacktrace --title \"My Site\" --start-page \"\" ")
