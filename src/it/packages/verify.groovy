File nodeDir = new File(basedir, "target/node")
assert nodeDir.isDirectory()
File siteIndexFile = new File(basedir, "target/site/test/1.0/index.html")
assert siteIndexFile.isFile()
File searchIndexFile = new File(basedir, "target/site/search-index.js")
assert searchIndexFile.isFile()
String stdout = new File(basedir, "build.log").text
assert stdout.contains("npx --yes --package antora --package @asciidoctor/tabs --package @antora/lunr-extension antora ")
