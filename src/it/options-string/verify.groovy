File nodeDir = new File(basedir, "target/node")
assert nodeDir.isDirectory()
File siteDir = new File(basedir, "target/my-site")
assert siteDir.isDirectory()
File siteIndexFile = new File(siteDir, "test/1.0/index.html")
assert siteIndexFile.isFile()
String indexText = siteIndexFile.text
assert indexText.contains("My Site")
assert indexText.contains("https://docs.example.org/test/1.0/index.html")
assert indexText.contains("The \"GOAT\" Service")
assert indexText.contains(">example.org<")
String stdout = new File(basedir, "build.log").text
// verifies that --stacktrace is added when error stacktraces are turned on in Maven
assert stdout.contains("antora --stacktrace --to-dir ${siteDir} --clean --title \"My Site\" --start-page \"\"")
