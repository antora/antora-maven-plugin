File providedPlaybookFile = new File(basedir, "provided-antora-playbook.yml")
assert providedPlaybookFile.isFile()
File siteIndexFile = new File(basedir, "target/site/test/1.0/index.html")
assert siteIndexFile.isFile()
String stdout = new File(basedir, "build.log").text
assert stdout.contains("Retrieving Antora playbook from provider")
assert stdout.contains("Antora playbook retrieved from local repository")
assert stdout.contains("--package antora@3.1.7")
assert stdout.contains("--package @asciidoctor/tabs")
