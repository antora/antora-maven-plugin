File nodeDir = new File(basedir, "target/node")
assert !nodeDir.exists()
File siteIndexFile = new File(basedir, "target/site/test/1.0/index.html")
assert !siteIndexFile.exists()
String stdout = new File(basedir, "build.log").text
assert stdout.contains("Skipping Antora site generation")
assert !stdout.contains("npx")
