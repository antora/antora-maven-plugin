package org.antora.maven;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.assertj.core.api.Assertions.assertThat;

public class FileUtilsTest {
    @TempDir
    File emptyDirectory;

    @TempDir
    File workDirectory;

    @Test
    void testIsEmptyDirectory() {
        assertThat(FileUtils.isEmptyDirectory(this.emptyDirectory)).isTrue();
    }

    @Test
    void testCleanDirectoryWhenDirectoryDoesNotExist() throws IOException {
        File subject = new File(this.workDirectory, "path/to/new-directory");
        FileUtils.cleanDirectory(subject);
        assertThat(FileUtils.isEmptyDirectory(subject)).isTrue();
    }

    @Test
    void testCleanDirectoryWhenInputIsFile() throws IOException {
        File subject = new File(this.workDirectory, "file-to-directory");
        FileUtils.cleanDirectory(subject);
        assertThat(FileUtils.isEmptyDirectory(subject)).isTrue();
    }

    @Test
    void testCleanDirectoryWithFiles() throws IOException {
        File subject = new File(this.workDirectory, "clean-directory-with-files");
        subject.mkdir();
        Files.writeString(new File(subject, "foo.txt").toPath(), "foo");
        Files.writeString(new File(subject, "bar.txt").toPath(), "bar");
        FileUtils.cleanDirectory(subject);
        assertThat(FileUtils.isEmptyDirectory(subject)).isTrue();
    }

    @Test
    void testCleanDirectoryWithDirectoriesWithFiles() throws IOException {
        File subject = new File(this.workDirectory, "clean-directory-with-nested-files");
        subject.mkdir();
        Files.writeString(new File(subject, "foo.txt").toPath(), "foo");
        new File(subject, "bar").mkdir();
        Files.writeString(new File(subject, "bar/baz.txt").toPath(), "baz");
        FileUtils.cleanDirectory(subject);
        assertThat(FileUtils.isEmptyDirectory(subject)).isTrue();
    }

    @Test
    void testCurrentDirectory() {
        String expected = System.getProperty("user.dir");
        assertThat(FileUtils.currentDirectory().getAbsolutePath()).isEqualTo(expected);
    }

    @Test
    void testDeleteDirectoryWhenDirectoryDoesNotExist() throws IOException {
        File subject = new File(this.workDirectory, "does-not-exist");
        FileUtils.deleteDirectory(subject);
        assertThat(subject.exists()).isFalse();
    }

    @Test
    void testDeleteDirectoryWhenInputIsFile() throws IOException {
        File subject = new File(this.workDirectory, "the-file");
        Files.writeString(subject.toPath(), "I exist");
        FileUtils.deleteDirectory(subject);
        assertThat(subject.exists()).isFalse();
    }

    @Test
    void testDeleteDirectoryWithFiles() throws IOException {
        File subject = new File(this.workDirectory, "delete-directory-with-files");
        subject.mkdir();
        Files.writeString(new File(subject, "foo.txt").toPath(), "foo");
        Files.writeString(new File(subject, "bar.txt").toPath(), "bar");
        FileUtils.deleteDirectory(subject);
        assertThat(subject.exists()).isFalse();
    }

    @Test
    void testDeleteDirectoryWithDirectoriesWithFiles() throws IOException {
        File subject = new File(this.workDirectory, "delete-directory-with-nested-files");
        subject.mkdir();
        Files.writeString(new File(subject, "foo.txt").toPath(), "foo");
        new File(subject, "bar").mkdir();
        Files.writeString(new File(subject, "bar/baz.txt").toPath(), "baz");
        FileUtils.deleteDirectory(subject);
        assertThat(subject.exists()).isFalse();
    }
}
