package org.antora.maven;

import java.nio.file.Path;
import java.util.regex.Pattern;

/**
 * A class that describes the current Node.js platform.
 */
public class Platform {
    private static final Pattern FRAGMENTED_COMMAND_LINE_ARGUMENT_RX = Pattern.compile("[ '\"]");

    private final boolean windows;

    /**
     * Instantiates this class.
     */
    public Platform() {
        this.windows = System.getProperty("os.name").toLowerCase().contains("windows");
    }

    /**
     * Resolves the specified command name for an npm package in accordance with the current platform.
     *
     * @param name - the raw command name to resolve
     *
     * @return the resolved command name
     */
    public String npmPackageCommandName(String name) {
        return this.windows ? name + ".cmd" : name;
    }

    /**
     * Escapes the command line argument in accordance with the current platform.
     *
     * @param value - a path
     *
     * @return the escaped value
     */
    public String escapeCommandLineArgument(Path value) {
        return escapeCommandLineArgument(value.toString());
    }

    /**
     * Escapes the command line argument in accordance with the current platform.
     *
     * @param value - any value
     *
     * @return the escaped value
     */
    public String escapeCommandLineArgument(String value) {
        if (value.isEmpty()) return "\"\"";
        if (!FRAGMENTED_COMMAND_LINE_ARGUMENT_RX.matcher(value).find()) return value;
        String q = "\"";
        if (value.contains(q)) {
            if (value.contains((q = "'"))) {
                // NOTE frontend plugin doesn't support escaped quotes; revert to character reference
                value = value.replace((q = "\""), "&quot;");
            } else if (this.windows) {
                value = value.replace("\"", "\"\"");
            }
        }
        return q + value + q;
    }
}
