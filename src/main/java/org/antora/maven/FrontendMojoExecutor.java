package org.antora.maven;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Plugin;
import org.apache.maven.model.PluginManagement;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.InvalidPluginDescriptorException;
import org.apache.maven.plugin.MojoExecution;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.PluginConfigurationException;
import org.apache.maven.plugin.PluginDescriptorParsingException;
import org.apache.maven.plugin.PluginManagerException;
import org.apache.maven.plugin.PluginNotFoundException;
import org.apache.maven.plugin.PluginResolutionException;
import org.apache.maven.plugin.descriptor.MojoDescriptor;
import org.apache.maven.plugin.descriptor.PluginDescriptor;
import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.configuration.PlexusConfiguration;
import org.codehaus.plexus.util.xml.Xpp3Dom;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.repository.RemoteRepository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

/**
 * A class that manages the execution of the mojo provided by the FrontEnd Maven Plugin.
 */
public class FrontendMojoExecutor {
    private static final String FRONTEND_MAVEN_PLUGIN_COORDINATES = "com.github.eirslett:frontend-maven-plugin:1.15.0";

    private static final String NPM_REGISTRY_URL_PARAMETER = "npmRegistryURL";

    private static final String NPM_INHERITS_PROXY_CONFIG_PARAMETER = "npmInheritsProxyConfigFromMaven";

    private final Log log;

    private final Xpp3Dom[] baseConfiguration;

    private final BuildPluginManager buildPluginManager;

    private final MavenSession mavenSession;

    private final PluginDescriptor pluginDescriptor;

    /**
     * Instantiates this class with the specified Maven log, build plugin manager, session, and configuration elements.
     *
     * @param log - the Maven log
     * @param buildPluginManager - the Maven build plugin manager
     * @param mavenSession - the Maven session
     * @param configurationElements - configuration elements for this mojo execution
     *
     * @throws MojoExecutionException - if the Frontend Maven Plugin cannot be loaded
     */
    public FrontendMojoExecutor(Log log, BuildPluginManager buildPluginManager, MavenSession mavenSession,
        Xpp3Dom... configurationElements) throws MojoExecutionException {
        this.log = log;
        this.baseConfiguration = configurationElements;
        this.pluginDescriptor = loadFrontendMavenPlugin(buildPluginManager, mavenSession);
        this.buildPluginManager = buildPluginManager;
        this.mavenSession = mavenSession;
    }

    /**
     * Execute the specified goal of the Frontend Maven plugin with the goal-scoped configuration elements.
     *
     * @param goal - the name of the goal to execute
     * @param configurationElements - configuration for this goal
     *
     * @throws MojoExecutionException - if the execution fails
     */
    public void executeMojo(String goal, Xpp3Dom... configurationElements) throws MojoExecutionException {
        MojoDescriptor mojoDescriptor = this.pluginDescriptor.getMojo(goal);
        if (mojoDescriptor == null) {
            throw new MojoExecutionException("Could not find goal " + goal + " in " + this.pluginDescriptor.getId());
        }
        Xpp3Dom configuration = buildConfiguration(mojoDescriptor, configurationElements);
        // suppress parameters of frontend-maven-plugin that break npx call (frontend-maven-plugin#1120)
        Xpp3Dom npmRegistryURL = configuration.getChild(NPM_REGISTRY_URL_PARAMETER);
        String npmRegistryURLOverride = null;
        if (npmRegistryURL != null) {
            npmRegistryURL.setValue("");
            npmRegistryURLOverride = System.clearProperty(NPM_REGISTRY_URL_PARAMETER);
            Xpp3Dom npmInheritsProxyConfig = configuration.getChild(NPM_INHERITS_PROXY_CONFIG_PARAMETER);
            if (npmInheritsProxyConfig != null) npmInheritsProxyConfig.setValue("false");
        }
        MojoExecution mojoExecution = new MojoExecution(mojoDescriptor, goal + "-antora");
        mojoExecution.setConfiguration(configuration);
        try {
            this.buildPluginManager.executeMojo(this.mavenSession, mojoExecution);
        } catch (MojoFailureException | PluginConfigurationException | PluginManagerException e) {
            String msg = "Unable to execute " + mojoDescriptor.getId();
            if (e instanceof MojoFailureException) {
                Throwable rootCause = e;
                while (rootCause.getCause() != null) rootCause = rootCause.getCause();
                if (rootCause.getMessage().endsWith("(Exit value: 1)")) {
                    msg += " (inspect prior log messages to find cause)";
                }
            }
            throw new MojoExecutionException(msg, e);
        } finally {
            if (npmRegistryURLOverride != null) System.setProperty(NPM_REGISTRY_URL_PARAMETER, npmRegistryURLOverride);
        }
    }

    /**
     * Respond to a Node.js version change.
     *
     * @param nodeHomeDir - the directory acting as the Node.js home directory
     * @param nodeVersion - the version of Node.js
     *
     * @throws MojoExecutionException - if the Node.js version change cannot be handled
     */
    public void handleNodeVersionChange(File nodeHomeDir, String nodeVersion) throws MojoExecutionException {
        File nodeVersionFile = new File(nodeHomeDir, ".node-version");
        if (FileUtils.hasContents(nodeVersionFile, nodeVersion)) return;
        try {
            FileUtils.cleanDirectory(nodeHomeDir);
            Files.writeString(nodeVersionFile.toPath(), nodeVersion);
        } catch (IOException e) {
            throw new MojoExecutionException("Failed to handle Node.js version change", e);
        }
    }

    private PluginDescriptor loadFrontendMavenPlugin(BuildPluginManager buildPluginManager, MavenSession mavenSession)
        throws MojoExecutionException {
        Plugin plugin = getFrontendMavenPlugin(mavenSession);
        List<RemoteRepository> repositories = mavenSession.getCurrentProject() == null
            ? List.of()
            : mavenSession.getCurrentProject().getRemotePluginRepositories();
        RepositorySystemSession repositorySession = mavenSession.getRepositorySession();
        try {
            return buildPluginManager.loadPlugin(plugin, repositories, repositorySession);
        } catch (PluginNotFoundException | PluginResolutionException | PluginDescriptorParsingException
            | InvalidPluginDescriptorException e) {
            throw new MojoExecutionException("Failed to load plugin " + plugin.getId(), e);
        }
    }

    private Plugin getFrontendMavenPlugin(MavenSession mavenSession) {
        String[] coordinates = FRONTEND_MAVEN_PLUGIN_COORDINATES.split(":", 3);
        Plugin result = new Plugin();
        result.setGroupId(coordinates[0]);
        result.setArtifactId(coordinates[1]);
        result.setVersion(coordinates[2]);
        PluginManagement pluginManagement = mavenSession.getCurrentProject().getPluginManagement();
        if (pluginManagement != null) {
            for (Plugin candidate : pluginManagement.getPlugins()) {
                if (candidate.getGroupId().equals(result.getGroupId()) &&
                    candidate.getArtifactId().equals(candidate.getArtifactId())) {
                    result.setVersion(candidate.getVersion());
                    break;
                }
            }
        }
        this.log.info("Using " + result.getId() + " to manage Node.js.");
        return result;
    }

    private Xpp3Dom buildConfiguration(MojoDescriptor mojoDescriptor, Xpp3Dom[] configurationElements) {
        Xpp3Dom configuration = toXpp3Dom(mojoDescriptor.getMojoConfiguration());
        mergeConfigurationElements(configuration, this.baseConfiguration);
        mergeConfigurationElements(configuration, configurationElements);
        return configuration;
    }

    private void mergeConfigurationElements(Xpp3Dom configuration, Xpp3Dom[] overrides) {
        Arrays.stream(overrides).forEach((it) -> {
            String name = it.getName();
            // NOTE must remove child by index for compatibility with Maven 3.6.3
            for (int i = 0, l = configuration.getChildCount(); i < l; i++) {
                Xpp3Dom candidate = configuration.getChild(i);
                if (name.equals(candidate.getName())) {
                    for (String attributeName : candidate.getAttributeNames()) {
                        if ("default-value".equals(attributeName)) continue;
                        it.setAttribute(attributeName, candidate.getAttribute(attributeName));
                    }
                    configuration.removeChild(i);
                    break;
                }
            }
            configuration.addChild(it);
        });
    }

    private Xpp3Dom toXpp3Dom(PlexusConfiguration configuration) {
        Xpp3Dom result = new Xpp3Dom(configuration.getName());
        for (String attributeName : configuration.getAttributeNames()) {
            result.setAttribute(attributeName, configuration.getAttribute(attributeName));
        }
        if (configuration.getChildCount() > 0) {
            for (PlexusConfiguration child : configuration.getChildren()) result.addChild(toXpp3Dom(child));
        } else {
            result.setValue(configuration.getValue());
        }
        return result;
    }
}
