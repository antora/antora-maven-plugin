package org.antora.maven;

import org.codehaus.plexus.classworlds.realm.ClassRealm;
import org.codehaus.plexus.component.configurator.BasicComponentConfigurator;
import org.codehaus.plexus.component.configurator.ComponentConfigurationException;
import org.codehaus.plexus.component.configurator.ConfigurationListener;
import org.codehaus.plexus.component.configurator.expression.ExpressionEvaluator;
import org.codehaus.plexus.configuration.PlexusConfiguration;

/**
 * Extends {@link BasicComponentConfigurator} to update the {@link PlexusConfiguration} object so that a user property
 * mapped to a collection parameter does not override the value set in the POM using child elements. Once this change is
 * applied, this class delegates back to the built-in behavior.
 */
public class AntoraPlexusComponentConfigurator extends BasicComponentConfigurator {
    @Override
    public void configureComponent(final Object component, final PlexusConfiguration configuration,
        final ExpressionEvaluator evaluator, final ClassRealm realm, final ConfigurationListener listener)
        throws ComponentConfigurationException {
        for (PlexusConfiguration child : configuration.getChildren()) {
            if (child.getChildCount() > 0) child.setValue(null);
        }
        super.configureComponent(component, configuration, evaluator, realm, listener);
    }
}
