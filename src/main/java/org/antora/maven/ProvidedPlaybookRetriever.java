package org.antora.maven;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * A class that manages the retrieval of the playbook file.
 */
public class ProvidedPlaybookRetriever {
    private static final String PACKAGES_MAGIC_COMMENT_PREFIX = "# PACKAGES ";

    private final Log log;

    private final File basedir;

    /**
     * Instantiates this class with the specified log object and base directory.
     *
     * @param log - the Maven log
     * @param basedir - the playbook directory
     */
    public ProvidedPlaybookRetriever(Log log, File basedir) {
        this.log = log;
        this.basedir = basedir;
    }

    /**
     * Retrieves the playbook file specified by the playbook provider if it has not yet been retrieved.
     *
     * @param provider - an object that describes the location of the playbook to retrieve
     * @param playbook - the local file to which the retrieved playbook contents should be written
     *
     * @return the map of dependent packages
     * @throws MojoExecutionException - if the playbook cannot be retrieved
     */
    public Map<String, PackageSpec> retrievePlaybook(PlaybookProvider provider, File playbook)
        throws MojoExecutionException {
        if (playbook.exists()) {
            log.info("Provided playbook has already been retrieved.");
            log.info("Remove " + playbook.getName() + " to retrieve a new copy.");
        } else {
            log.info("Retrieving Antora playbook from provider.");
            boolean tryDownload = provider.isDownloadAvailable();
            if (provider.isSideloadAvailable()) {
                try {
                    new FileSideloader(this.basedir).sideload(provider.getRevPath(), playbook);
                    tryDownload = false;
                    log.info("Antora playbook retrieved from local repository.");
                } catch (IOException e) {
                    playbook.delete();
                    if (!tryDownload) {
                        String msg =
                            "Unable to find provided playbook in local repository and no remote repository is specified from which to download it";
                        log.error(msg);
                        throw new MojoExecutionException(msg, e);
                    }
                }
            }
            if (tryDownload) {
                try {
                    new FileDownloader().download(provider.getDownloadURL(), playbook);
                    log.info("Antora playbook retrieved from remote repository.");
                } catch (IOException e) {
                    playbook.delete();
                    String msg = "Unable to retrieve provided playbook from remote repository.";
                    log.error(msg);
                    throw new MojoExecutionException(msg, e);
                }
            }
        }
        return extractPackages(playbook);
    }

    private Map<String, PackageSpec> extractPackages(File playbook) {
        Map<String, PackageSpec> packages = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(playbook))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.startsWith(PACKAGES_MAGIC_COMMENT_PREFIX)) continue;
                String[] specs = line.stripTrailing().substring(PACKAGES_MAGIC_COMMENT_PREFIX.length()).split(" +");
                for (String spec : specs) {
                    PackageSpec pkg = PackageSpec.valueOf(spec);
                    packages.put(pkg.getName(), pkg);
                }
                break;
            }
        } catch (IOException e) {}
        return packages;
    }
}
