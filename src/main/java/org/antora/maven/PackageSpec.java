package org.antora.maven;

import java.util.Objects;

/**
 * A class that represents an npm package spec.
 */
public class PackageSpec {
    private final String name;

    private final String versionSpec;

    /**
     * Instantiates this class with the specified package name and null version.
     *
     * @param name - the name of the package
     */
    public PackageSpec(String name) {
        this(name, null);
    }

    /**
     * Instantiates this class with the specified package name and version spec.
     *
     * @param name - the name of the package
     * @param versionSpec - the version spec of the package (e.g., "3", "~3.1", "latest", etc)
     */
    public PackageSpec(String name, String versionSpec) {
        if (name == null) throw new IllegalArgumentException("name cannot be null");
        if (name.isEmpty()) throw new IllegalArgumentException("name cannot be empty");
        this.name = name;
        this.versionSpec = versionSpec;
    }

    /**
     * Gets the name of this package
     *
     * @return the name of the package
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns the package spec as a string in the form name or name@version if the version is not null.
     *
     * @return the package spec as a string
     */
    public String toString() {
        if (this.versionSpec == null || "latest".equals(this.versionSpec)) return this.name;
        return this.name + "@" + this.versionSpec;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        return this.name.equals(((PackageSpec) other).name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.name);
    }

    /**
     * Instantiates this class from its string representation.
     *
     * @param packageSpec - the string representation of a package spec
     *
     * @return the package spec as an instance of this class
     */
    public static PackageSpec valueOf(String packageSpec) {
        if (packageSpec.indexOf("@", 1) < 0) return new PackageSpec(packageSpec);
        String[] nameAndVersionSpec = packageSpec.split("(?!^)@", 2);
        return new PackageSpec(nameAndVersionSpec[0], nameAndVersionSpec[1]);
    }
}
