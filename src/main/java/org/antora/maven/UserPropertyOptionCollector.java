package org.antora.maven;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Collects options from user properties that have a designated prefix.
 */
public class UserPropertyOptionCollector {
    private static final Pattern INDEXED_PROPERTY_NAME_RX = Pattern.compile("([^\\[]+)\\[([0-9]+)\\]");

    private final String prefix;

    private final String command;

    /**
     * Instantiates this class with the specified prefix.
     * 
     * @param prefix - the prefix for identifying qualifying options (e.g., "antora.option.")
     */
    public UserPropertyOptionCollector(String prefix) {
        this.prefix = prefix;
        this.command = System.getProperty("sun.java.command");
    }

    /**
     * Collect options from user properties that start with the prefix configured on this object.
     * 
     * If the name is indexed, [] will be appended to the name in the collected option.
     *
     * @param userProperties - the properties to scan
     *
     * @return the collected options in name=value format with the prefix removed
     */
    public List<String> collectOptions(Properties userProperties) {
        List<String> options = new ArrayList<>();
        int startIdx = this.prefix.length();
        for (String name : getSortedPropertyNames(userProperties)) {
            if (!name.startsWith(this.prefix) || name.indexOf(".", startIdx) > -1) continue;
            int endIdx = name.endsWith("]") ? name.indexOf("[", startIdx) : -1;
            if (endIdx == -1) {
                String value = userProperties.getProperty(name);
                options.add(name.substring(startIdx) + (isNameOnlyUserProperty(name, value) ? "" : "=" + value));
            } else {
                options.add(name.substring(startIdx, endIdx) + "[]=" + userProperties.getProperty(name));
            }
        }
        return options;
    }

    private List<String> getSortedPropertyNames(Properties properties) {
        List<String> propertyNames = new ArrayList<>(properties.stringPropertyNames());
        propertyNames.sort((a, b) -> {
            if (a.endsWith("]") && b.endsWith("]")) {
                Matcher aMatcher = INDEXED_PROPERTY_NAME_RX.matcher(a);
                if (aMatcher.matches() && b.startsWith(aMatcher.group(1))) {
                    Matcher bMatcher = INDEXED_PROPERTY_NAME_RX.matcher(b);
                    if (bMatcher.matches() && aMatcher.group(1).equals(bMatcher.group(1))) {
                        return Integer.valueOf(aMatcher.group(2)).compareTo(Integer.valueOf(bMatcher.group(2)));
                    }
                }
            }
            return a.compareTo(b);
        });
        return propertyNames;
    }

    /**
     * Checks whether the user property with a value "true" is actually valueless.
     */
    private boolean isNameOnlyUserProperty(String name, String value) {
        if (!"true".equals(value)) return false;
        if (this.command == null) return !name.endsWith("?") && !name.endsWith("]");
        return (this.command + " ").contains(" -D" + name + " ");
    }
}
