package org.antora.maven;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class that describes how to locate and retrieve a provided playbook.
 */
public class PlaybookProvider {
    private static final Map<String, String> DOWNLOAD_URL_TEMPLATES =
        new HashMap<>(Map.of("github.com", "https://raw.githubusercontent.com/%s/%s/%s", "gitlab.com",
            "https://gitlab.com/%s/-/raw/%s/%s", "bitbucket.org", "https://bitbucket.org/%s/raw/%s/%s"));

    private static final Map<String, String> HOST_ALIASES =
        new HashMap<>(Map.of("github", "github.com", "gitlab", "gitlab.com", "bitbucket", "bitbucket.org"));

    private static final List<String> TRY_LOCAL_BRANCH_VALUES = List.of("false", "first", "only", "never");

    private String host;

    private String repository;

    private String branch;

    private String path;

    private String tryLocalBranch;

    /**
     * Instantiates this class with default values.
     */
    public PlaybookProvider() {
        this.host = "github.com";
        this.repository = null;
        this.branch = "docs-build";
        this.path = "antora-playbook-template.yml";
        this.tryLocalBranch = null;
    }

    /**
     * Get the host where the playbook is located.
     *
     * @return the host (e.g., "github.com")
     */
    public String getHost() {
        return this.host;
    }

    /**
     * Set the host where the playbook is located.
     *
     * @param host - the host (e.g., "github.com")
     */
    public void setHost(String host) {
        this.host = HOST_ALIASES.getOrDefault(host, host);
    }

    /**
     * Get the git repository where the playbook is located.
     *
     * @return the name of the repository
     */
    public String getRepository() {
        return this.host == null ? null : this.repository;
    }

    /**
     * Set the git repository where the playbook is located.
     *
     * @param repository - the name of the repository; may be used to specify the host followed by a colon
     */
    public void setRepository(String repository) {
        if (repository != null && repository.indexOf(':') > 0) {
            String[] hostAndRepository = repository.split(":", 2);
            setHost(hostAndRepository[0]);
            this.repository = hostAndRepository[1];
        } else {
            this.repository = repository;
        }
    }

    /**
     * Get the branch of the repository where the playbook is located.
     *
     * @return the name of the branch
     */
    public String getBranch() {
        return this.branch;
    }

    /**
     * Set the branch of the repository where the playbook is located.
     *
     * @param branch - the name of the branch
     */
    public void setBranch(String branch) {
        this.branch = branch;
    }

    /**
     * Get the path in the repository where the playbook is located.
     *
     * @return the path
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Set the path in the repository where the playbook is located.
     *
     * @param path - the path; may be used to specify the branch followed by a colon
     */
    public void setPath(String path) {
        if (path != null && path.indexOf(':') > 0) {
            String[] branchAndPath = path.split(":", 2);
            setBranch(branchAndPath[0]);
            this.path = branchAndPath[1];
        } else {
            this.path = path;
        }
    }

    /**
     * Get the configuration option that controls when to look for the branch locally.
     *
     * @return an enum that specifies when to look for the branch locally
     */
    public String getTryLocalBranch() {
        return this.tryLocalBranch;
    }

    /**
     * Set the configuration option that controls when to look for the branch locally.
     *
     * @param tryLocalBranch - an enum that specifies when to look for the branch locally; valid values are null,
     * "false", "first", "only", or "never"
     */
    public void setTryLocalBranch(String tryLocalBranch) {
        if (tryLocalBranch == null) {
            this.tryLocalBranch = null;
        } else if (TRY_LOCAL_BRANCH_VALUES.contains(tryLocalBranch)) {
            this.tryLocalBranch = "false".equals(tryLocalBranch) ? "never" : tryLocalBranch;
        } else {
            this.tryLocalBranch = "first";
        }
    }

    /**
     * Reports whether the playbook can be downloaded.
     *
     * @return whether the playbook can be downloaded
     */
    public boolean isDownloadAvailable() {
        return !"only".equals(getTryLocalBranch()) && getRepository() != null &&
            DOWNLOAD_URL_TEMPLATES.containsKey(getHost());
    }

    /**
     * Reports whether the playbook can be sideloaded.
     *
     * @return whether the playbook can be sideloaded
     */
    public boolean isSideloadAvailable() {
        return !"never".equals(getTryLocalBranch()) && (getRepository() == null || getTryLocalBranch() != null);
    }

    /**
     * Resolves the download URL by filling in the template with the properties specified on this provider.
     *
     * @return the download URL
     * @throws MalformedURLException - if the resolved URL is malformed
     */
    public URL getDownloadURL() throws MalformedURLException {
        return new URL(String.format(DOWNLOAD_URL_TEMPLATES.get(getHost()), getRepository(), getBranch(), getPath()));
    }

    /**
     * Gets the rev path (branch:path) where the playbook is located
     *
     * @return the rev path
     */
    public String getRevPath() {
        return getBranch() + ':' + getPath();
    }

    /**
     * Validates the specified playbook provider
     *
     * @param candidate - the playbook provider to validate
     *
     * @return the valid playbook provider, otherwise null
     */
    public static PlaybookProvider validate(PlaybookProvider candidate) {
        if (candidate == null || candidate.getBranch() == null || candidate.getPath() == null) return null;
        if (candidate.isSideloadAvailable() || candidate.isDownloadAvailable()) return candidate;
        return null;
    }
}
