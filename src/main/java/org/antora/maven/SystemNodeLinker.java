package org.antora.maven;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.logging.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.nio.file.Files.createSymbolicLink;
import static java.nio.file.Files.readSymbolicLink;

/**
 * A class that manages the symbolic links to a system-wide Node.js installation.
 */
public class SystemNodeLinker {
    private static final String RESOLVE_NODE_HOME_SCRIPT = "const p=require('path');" +
        "const exe=process.execPath,root=p.join(exe,p.basename(p.dirname(exe))==='bin'?'../..':'..');" +
        "function hasNpm(v){try{require.resolve(p.join((v=p.join(root,v)),'npm/bin/npm-cli.js'));return v}catch{}};" +
        "const pkgs=hasNpm('lib/node_modules')||hasNpm('node_modules')||hasNpm('libexec/lib/node_modules')||'';" +
        "pkgs&&exe+'\\n'+pkgs+'\\n'+process.version";

    private final Log log;

    private final Path symbolicNodeHomeDir;

    /**
     * Instantiates this class with the specified log object and location where the symbolic links should be created
     *
     * @param log - the Maven log
     * @param symbolicNodeHomeDir - the location that acts as the Node.js home dir where the links should be created
     */
    public SystemNodeLinker(Log log, File symbolicNodeHomeDir) {
        this.log = log;
        this.symbolicNodeHomeDir = symbolicNodeHomeDir.toPath();
    }

    /**
     * Creates symlinks to the Node.js installation for the specified executable.
     *
     * @param nodeExecutable - the Node.js executable that identifies the Node.js installation
     *
     * @return the linked Node.js version
     * @throws MojoExecutionException - if the Node.js installation is invalid or the symbolic links cannot be created
     */
    public String linkNode(String nodeExecutable) throws MojoExecutionException {
        List<String> targets = null;
        String quotedNodeExecutable = "nodeExecutable \"" + nodeExecutable + "\"";
        try {
            if ((targets = resolveSystemNodeHome(nodeExecutable)).isEmpty()) targets = null;
        } catch (IOException e) {
            String msg = "Cannot run " + quotedNodeExecutable;
            Throwable cause = e.getCause() == null ? e : e.getCause();
            log.error(msg + ": " + cause.getMessage());
            throw new MojoExecutionException(msg, cause);
        }
        if (targets == null) {
            String msg = "Cannot verify compatible Node.js installation for " + quotedNodeExecutable;
            log.error(msg);
            throw new MojoExecutionException(msg);
        }
        Path nodeExecutableTarget = Path.of(targets.get(0));
        Path nodeModulesDirectoryTarget = Path.of(targets.get(1));
        String nodeVersion = targets.get(2);
        Path nodeExecutableLink = this.symbolicNodeHomeDir.resolve("node" + getFileExtension(nodeExecutableTarget));
        Path nodeModulesDirectoryLink = this.symbolicNodeHomeDir.resolve("node_modules");
        if (isSymbolicLinkTo(nodeExecutableLink, nodeExecutableTarget) &&
            isSymbolicLinkTo(nodeModulesDirectoryLink, nodeModulesDirectoryTarget)) {
            log.info("System Node.js already linked for " + quotedNodeExecutable);
            return nodeVersion;
        }
        try {
            FileUtils.cleanDirectory(this.symbolicNodeHomeDir.toFile());
        } catch (IOException e) {
            String msg = "Unable to prepare Node.js installation directory for " + quotedNodeExecutable;
            log.error(msg);
            throw new MojoExecutionException(msg, e);
        }
        try {
            createSymbolicLink(nodeExecutableLink, nodeExecutableTarget);
            createSymbolicLink(nodeModulesDirectoryLink, nodeModulesDirectoryTarget);
            log.info("Linking system Node.js for " + quotedNodeExecutable);
            return nodeVersion;
        } catch (IOException e) {
            // likely cause is missing permission to use mklink on Windows; fallback to copy operation
            try {
                Files.copy(nodeExecutableTarget, nodeExecutableLink, StandardCopyOption.COPY_ATTRIBUTES);
                File npmModuleDirectorySrc = new File(nodeModulesDirectoryTarget.toFile(), "npm");
                File npmModuleDirectoryDest = new File(nodeModulesDirectoryLink.toFile(), "npm");
                npmModuleDirectoryDest.getParentFile().mkdir();
                FileUtils.copyRecursively(npmModuleDirectorySrc, npmModuleDirectoryDest);
                log.info("Copying system Node.js for " + quotedNodeExecutable);
                return nodeVersion;
            } catch (IOException re) {
                e = re;
            }
            String msg = "Unable to link or copy system Node.js for " + quotedNodeExecutable;
            log.error(msg);
            throw new MojoExecutionException(msg, e);
        }
    }

    /**
     * Removes the symbolic links that point to the system installation of Node.js.
     */
    public void unlinkNode() {
        if (!this.symbolicNodeHomeDir.toFile().isDirectory()) return;
        List.of("node", "node.exe", "node_modules").forEach(name -> {
            Path path = this.symbolicNodeHomeDir.resolve(name);
            if (isSymbolicLink(path)) path.toFile().delete();
        });
    }

    private List<String> resolveSystemNodeHome(String nodeExecutable) throws IOException {
        Process p = new ProcessBuilder(nodeExecutable, "-p", RESOLVE_NODE_HOME_SCRIPT).start();
        try (BufferedReader stdoutReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stderrReader = new BufferedReader(new InputStreamReader(p.getErrorStream()))) {
            List<String> output = stdoutReader.lines().collect(Collectors.toList());
            if (output.size() == 1 && output.get(0).isEmpty()) output.clear();
            int exitValue;
            try {
                p.waitFor(1, TimeUnit.SECONDS);
                exitValue = p.exitValue();
            } catch (InterruptedException e) {
                exitValue = 1;
            }
            if (exitValue == 0 && (output.size() == 3 || output.isEmpty())) return output;
            String msg = "Not a valid Node.js executable";
            IOException cause = null;
            if (exitValue > 0) {
                List<String> errLines = stderrReader.lines().collect(Collectors.toList());
                if (!errLines.isEmpty() &&
                    !Pattern.compile(" (?:flag|option)[: ].*[-']?p").matcher(errLines.get(0)).find()) {
                    cause = new IOException("error=" + exitValue + ", " + String.join("\n", errLines));
                }
            }
            throw new IOException(msg, cause);
        }
    }

    private String getFileExtension(Path path) {
        String fileName = path.getFileName().toString();
        int fileExtensionIdx = fileName.lastIndexOf('.');
        if (fileExtensionIdx < 0) return "";
        return fileName.substring(fileExtensionIdx);
    }

    private boolean isSymbolicLink(Path candidate) {
        try {
            readSymbolicLink(candidate);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private boolean isSymbolicLinkTo(Path candidate, Path target) {
        try {
            return readSymbolicLink(candidate).equals(target);
        } catch (IOException e) {
            return false;
        }
    }
}
