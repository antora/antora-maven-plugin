package org.antora.maven;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;
import java.util.concurrent.CompletionException;
import java.util.stream.Stream;

/**
 * A utility class that provides helpers for operating on files and directories.
 */
public class FileUtils {
    /**
     * Cleans the specified directory and ensures that an empty directory exists at this location.
     *
     * If the specified file is not a directory, it is first removed.
     *
     * @param directory - the directory to clean
     *
     * @throws IOException - if the directory cannot be cleaned
     */
    public static void cleanDirectory(File directory) throws IOException {
        if (!directory.isDirectory()) {
            Files.deleteIfExists(directory.toPath());
            directory.mkdirs();
            return;
        }
        try (Stream<Path> walk = Files.walk(directory.toPath()).sorted(Comparator.reverseOrder())) {
            walk.map(Path::toFile).filter(file -> !file.equals(directory)).forEach(File::delete);
        }
    }

    /**
     * Copy all files recursively from the source to the destination.
     *
     * @param src - the file or directory to copy
     * @param dest - the destination file or directory
     *
     * @throws IOException - if the files cannot be copied
     */
    public static void copyRecursively(File src, File dest) throws IOException {
        int relativePathOffset = src.getPath().length();
        try (Stream<Path> walk = Files.walk(src.toPath())) {
            try {
                walk.forEach((file) -> {
                    String relativePath = file.toString().substring(relativePathOffset);
                    try {
                        Files.copy(file, Path.of(dest.getPath(), relativePath), StandardCopyOption.COPY_ATTRIBUTES);
                    } catch (IOException e) {
                        throw new CompletionException(e);
                    }
                });
            } catch (CompletionException e) {
                throw (IOException) e.getCause();
            }
        }
    }

    /**
     * Resolves the current directory
     *
     * @return the current directory
     */
    public static File currentDirectory() {
        return Path.of("").toAbsolutePath().toFile();
    }

    /**
     * Deletes the specified directory.
     *
     * @param directory - the directory (or file) to delete
     *
     * @return whether the specified direcotry was deleted
     * @throws IOException - if the directory cannot be deleted
     */
    public static boolean deleteDirectory(File directory) throws IOException {
        if (!directory.isDirectory()) return Files.deleteIfExists(directory.toPath());
        try (Stream<Path> walk = Files.walk(directory.toPath()).sorted(Comparator.reverseOrder())) {
            walk.map(Path::toFile).forEach(File::delete);
        }
        return true;
    }

    /**
     * Checks whether the specified file has the specified contents
     *
     * @param file - the file to check
     * @param contents - the contents to check for
     *
     * @return whether the specified file contains the specified contents
     */
    public static boolean hasContents(File file, String contents) {
        if (!file.exists()) return false;
        try {
            return contents.equals(Files.readString(file.toPath()).stripTrailing());
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Checks whether the specified directory is an empty directory
     *
     * @param directory - the directory to check
     *
     * @return whether the specified directory is an empty directory
     */
    public static boolean isEmptyDirectory(File directory) {
        if (!directory.isDirectory()) return false;
        try (Stream<Path> entries = Files.list(directory.toPath())) {
            return entries.findFirst().isEmpty();
        } catch (IOException e) {
            return false;
        }
    }
}
