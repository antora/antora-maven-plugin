package org.antora.maven;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

/**
 * A utility class that provides a helper for downloading the contents of a URL to a local file.
 */
public class FileDownloader {
    /**
     * Downloads the contents of the specified URL to the specified File.
     *
     * @param url - the URL to download
     * @param toFile - the local file in which to store the contents of the URL
     *
     * @throws IOException - if the operation fails
     */
    public void download(URL url, File toFile) throws IOException {
        try (BufferedInputStream in = new BufferedInputStream(url.openStream());
            FileOutputStream out = new FileOutputStream(toFile)) {
            byte[] buffer = new byte[1024];
            int count = 0;
            while ((count = in.read(buffer, 0, 1024)) != -1) out.write(buffer, 0, count);
        }
    }
}
