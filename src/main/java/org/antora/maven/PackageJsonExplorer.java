package org.antora.maven;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Map;

/**
 * A class that provides helper methods for checking the status and contents of the npm package file.
 */
public class PackageJsonExplorer {
    private static final String PACKAGE_JSON_NAME = "package.json";

    private static final String PACKAGE_LOCKFILE_NAME = "package-lock.json";

    private final File directory;

    private final File packageFile;

    private final File packageLockfile;

    /**
     * Instantiates this class to operate on the specified directory.
     *
     * @param directory - the location of the npm package file and npm package lock file
     */
    public PackageJsonExplorer(File directory) {
        this.directory = directory;
        this.packageFile = new File(directory, PACKAGE_JSON_NAME);
        this.packageLockfile = new File(directory, PACKAGE_LOCKFILE_NAME);
    }

    /**
     * Checks whether the npm package file exists with any dependencies.
     *
     * @return whether the npm package file exists and has at least one dependency
     */
    public boolean hasPackageFileWithDependencies() {
        if (!this.packageFile.exists()) return false;
        try (FileReader reader = new FileReader(this.packageFile)) {
            JsonObject pkg = JsonParser.parseReader(reader).getAsJsonObject();
            return (pkg.has("dependencies") && !pkg.getAsJsonObject("dependencies").isEmpty()) ||
                (pkg.has("devDependencies") && !pkg.getAsJsonObject("devDependencies").isEmpty());
        } catch (IOException e) {
            return true;
        }
    }

    /**
     * Checks whether the npm package lock file exists (regardless of whether the npm package file is present).
     *
     * @return whether the npm package lock file exists
     */
    public boolean hasLockfile() {
        return this.packageLockfile.exists();
    }

    /**
     * Checks whether the bin script with the specified name is present relative to this npm package file.
     *
     * @param name - the name of the bin script to look for
     *
     * @return whether the bin script exists
     */
    public boolean isBinScriptInstalled(String name) {
        return this.directory.toPath().resolve(Path.of("node_modules", ".bin", name)).toFile().exists();
    }

    /**
     * Finds the declared version for the specified package.
     *
     * @param packageName - the name of the package to check
     * @param fromLockFile - whether to consult the lock file
     *
     * @return the declared version for the specified package
     */
    public String getPackageVersion(String packageName, boolean fromLockFile) {
        try (FileReader reader = new FileReader(fromLockFile ? this.packageLockfile : this.packageFile)) {
            JsonObject pkg = JsonParser.parseReader(reader).getAsJsonObject();
            String version = null;
            if (fromLockFile) {
                version = getPackageVersion(pkg, "packages", packageName);
            } else {
                version = getPackageVersion(pkg, "dependencies", packageName);
                if (version == null) version = getPackageVersion(pkg, "devDependencies", packageName);
            }
            return version == null || version.contains(":") ? null : version;
        } catch (IOException e) {
            return null;
        }
    }

    private String getPackageVersion(JsonObject pkg, String property, String packageName) {
        JsonObject category = pkg.getAsJsonObject(property);
        if (category == null) return null;
        boolean fromLockFile = "packages".equals(property);
        if (fromLockFile) packageName = "node_modules/" + packageName;
        for (Map.Entry<String, JsonElement> entry : category.entrySet()) {
            if (!packageName.equals(entry.getKey())) continue;
            JsonElement value = entry.getValue();
            if (fromLockFile) value = value.getAsJsonObject().get("version");
            return value.getAsString();
        }
        return null;
    }
}
