package org.antora.maven;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.maven.plugin.logging.Log;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * A class that handles resolving the Node.js version.
 */
public class NodeVersionResolver {
    private static String FALLBACK_VERSION = "v16.20.2";

    private static String RELEASES_DATA_URL = "https://nodejs.org/dist/index.json";

    private Log log;

    private FileDownloader fileDownloader;

    private File releasesDataCache;

    private URL releasesDataSource;

    /**
     * Instantiates this class with the Maven log and Node.js download URL.
     *
     * @param log - the Maven log
     * @param nodeDownloadUrl - the URL where the Node.js releases data is located.
     */
    public NodeVersionResolver(Log log, URL nodeDownloadUrl) {
        this.log = log;
        this.fileDownloader = new FileDownloader();
        this.releasesDataCache = resolveReleasesCache();
        this.releasesDataSource = toURL(RELEASES_DATA_URL);
    }

    /**
     * Resolves the Node.js version from the version string, using the default fallback version otherwise.
     *
     * @param version - the version spec (an exact version, a keyword, or a fuzzy version)
     *
     * @return the resolved version
     */
    public String resolveVersion(String version) {
        return resolveVersion(version, FALLBACK_VERSION);
    }

    /**
     * Resolves the Node.js version from the version string, using the specified fallback version otherwise.
     *
     * @param version - the version spec (an exact version, a keyword, or a fuzzy version)
     * @param fallbackVersion - an exact version to use as a fallback
     *
     * @return the resolved version
     */
    public String resolveVersion(String version, String fallbackVersion) {
        boolean offline = false;
        String requestedVersion = version;
        if (version == null || version.isEmpty()) {
            version = requestedVersion = "lts";
        } else {
            if (version.startsWith("v") && version.length() > 1 && version.substring(1, 2).matches("\\d")) {
                version = version.substring(1);
            }
            if (version.split("[.]").length > 2) return "v" + version;
        }
        if ("lts".equals(version) && System.getProperty("org.apache.maven.test.invoker") != null) {
            return System.getProperty("node.lts.version");
        }
        String resolvedVersion;
        if (fallbackVersion.startsWith("v")) fallbackVersion = fallbackVersion.substring(1);
        try {
            boolean releasesDataCached = this.releasesDataCache.exists();
            if (offline) {
                if (!releasesDataCached) {
                    this.log.warn("No cached version of Node.js release data available to resolve Node.js version " +
                        "while in offline mode. Reverting to fallback version.");
                }
            } else if (!releasesDataCached || isOlderThanOneDay(this.releasesDataCache.lastModified())) {
                if (!releasesDataCached) this.releasesDataCache.getParentFile().mkdirs();
                fileDownloader.download(this.releasesDataSource, this.releasesDataCache);
            }
            JsonArray releases;
            try (FileReader reader = new FileReader(this.releasesDataCache)) {
                releases = JsonParser.parseReader(reader).getAsJsonArray();
            }
            Stream<JsonObject> releasesStream =
                StreamSupport.stream(releases.spliterator(), false).map(JsonElement::getAsJsonObject).map(it -> {
                    String rawVersion = it.get("version").getAsString();
                    if (rawVersion.startsWith("v")) {
                        it.remove("version");
                        it.addProperty("version", rawVersion.substring(1));
                    }
                    return it;
                });
            Optional<JsonObject> release;
            if (version.equals("latest")) {
                release = releasesStream.findFirst();
            } else if (version.equals("lts")) {
                release = releasesStream.filter(it -> it.getAsJsonPrimitive("lts").isString()).findFirst();
            } else {
                String prefix = version + ".";
                release = releasesStream.filter(it -> it.get("version").getAsString().startsWith(prefix)).findFirst();
            }
            if (release.isPresent()) {
                resolvedVersion = release.get().get("version").getAsString();
            } else {
                this.log.warn("Could not resolve version " + requestedVersion + " in Node.js releases data. " +
                    "Reverting to fallback version.");
                resolvedVersion = fallbackVersion;
            }
        } catch (IOException e) {
            resolvedVersion = fallbackVersion;
        }
        return "v" + resolvedVersion;
    }

    private File resolveReleasesCache() {
        String appDataDir = System.getenv("APPDATA");
        if (appDataDir == null) {
            String homeDir = System.getProperty("user.home");
            appDataDir = System.getProperty("os.name").contains("darwin")
                ? Path.of(homeDir, "Library", "Preferences").toString()
                : Path.of(homeDir, ".local", "share").toString();
        }
        return Path.of(appDataDir, "node", "releases.json").toFile();
    }

    private boolean isOlderThanOneDay(long lastModified) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR, -24);
        Date dayOld = cal.getTime();
        return new Date(lastModified).before(dayOld);
    }

    private URL toURL(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
}
