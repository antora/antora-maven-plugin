= Use the Antora Maven Plugin
:navtitle: Usage
:description: To run Antora from a Maven project, add the Antora Maven plugin to your build and register the custom lifecycle is via the extensions tag.

== System requirements

The Antora Maven plugin requires at least Java {version-java-major} and Maven {version-maven-min} (though Maven {version-maven-minor} is recommended).
You don't need Node.js preinstalled to use this plugin.
The plugin will set up a Node.js runtime for you automatically (controlled through the plugin configuration).

The following sections cover how to configure and use the `antora:antora` goal.

[#apply]
== Apply the plugin

The Antora Maven plugin is published to {url-artifact}[Maven Central^].
Therefore, to use it, you can declare it in your project's POM file without having to set up any additional repositories.

To start, declare the plugin in the POM for the project (or one of its modules) that contains the Antora playbook.

.pom.xml
[,xml,subs=attributes+]
----
<plugin>
  <groupId>org.antora</groupId>
  <artifactId>antora-maven-plugin</artifactId>
  <version>{release-version}</version>
</plugin>
----

This declaration will add the `antora:antora` goal to your project.
In other words, it adds a goal named `antora` which has the plugin prefix `antora`.
This goal is not bound to any lifecycle phase by default.
That means you must invoke it directly when running Maven (e.g., `mvn antora:antora`).

[#add-lifecycle]
=== Add the antora lifecycle (optional)

If you want to be able abbreviate `antora:antora` as `antora`, then you need to declare that the plugin has extensions using the `<extensions>true</extensions>` tag.

.pom.xml
[,xml,subs=attributes+]
----
<plugin>
  <groupId>org.antora</groupId>
  <artifactId>antora-maven-plugin</artifactId>
  <version>{release-version}</version>
  <extensions>true</extensions> <!--1-->
</plugin>
----
<1> Registers the plugin with extensions enabled.

By enabling the plugin's extensions, the plugin will add the `antora` lifecycle, which introduces a new phase named `antora`.
The `antora:antora` goal is bound to this phase.
That's why you can run Antora using `mvn antora` in place of `mvn antora:antora`.

NOTE: Thoughout the documentation, whenever you see `antora:antora`, it can be abbreviated as `antora` if `<extensions>true</extension>` is set in the plugin declaration.

Adding the `antora` lifecycle allows you to bind other plugins to its phases for performing work with or around the Antora call.
See xref:antora-lifecycle.adoc[] for details on what this entails.

[#frontend-maven-plugin-version]
=== Override the frontend-maven-plugin version

By default, the Antora Maven plugin uses a predefined version of the frontend-maven-plugin (the same version used to test this plugin).
To override the frontend-maven-plugin version, add an entry for the frontend-maven-plugin in the `<pluginManagement>` section in your Maven build.
For example:

.pom.xml
[,xml]
----
<pluginManagement>
  <plugins>
    <plugin>
      <groupId>com.github.eirslett</groupId>
      <artifactId>frontend-maven-plugin</artifactId>
      <version>1.15.0</version>
    </plugin>
  </plugins>
</pluginManagement>
----

[#run]
== Run Antora using the antora goal

You can now invoke Antora using the xref:antora-goal.adoc[antora goal] to build your site.

 $ mvn antora:antora

If you've enabled extensions on the plugin, the goal can be replaced with the phase name.

 $ mvn antora

You can find the output that Antora generates under the project's build directory.

[,bash]
----
$ tree target/site -P '*.html'
├── _
│   ├── css
│   ├── font
│   ├── img
│   └── js
│       └── vendor
└── test
    └── 1.0.0-SNAPSHOT
        └── index.html
----

If the current project has modules (i.e., a multi-module project), you may need to add the `-N` (non-recursive) option to prevent the goal from running on the other projects.

 $ mvn antora:antora -N

You can use the `-pl .` (project list) option in place of `-N` to accomplish the same task.

 $ mvn antora:antora -pl .

If you've created a custom execution profile and want to activate it, append `@` followed by the execution ID to the goal name.

 $ mvn antora:antora@production

You can specify additional Antora and Node.js options in the xref:antora-goal.adoc#configuration-parameters[configuration of the plugin].

If you get errors running the plugin after changing the configuration, clean the project using `mvn clean`, then try running the goal again.
If you're using [.path]_package.json_ to manage packages, try removing the [.path]_node_modules_ folder as well.

[#run-standalone]
=== Run Antora outside a Maven project

It's possible to run the `antora:antora` goal outside of a Maven project.
To do so, you must reference the goal name using its fully qualified artifact name (i.e., `<groupId>:<artifactId>` or `<groupId>:<artifactId>:<version>`) in place of the goal prefix `antora`.
If no version is specified, Maven will select the latest version.

[subs=attributes+]
 $ mvn org.antora:antora-maven-plugin:{release-version}:antora

The plugin will be downloaded and installed into your local Maven repository on demand.
When the plugin goal is used this way, the site will be built to the output directory specified in the playbook, otherwise Antora's default location, which is [.path]_build/site_.
Node.js will be set up (installed or linked) in the [.path]_node_ directory relative to the current directory.

You can configure the `antora:antora` goal using the user properties described in the xref:antora-goal.adoc#configuration-parameters[configuration parameters].

== Examples

[#link-failure]
=== Fail the build when Antora fails

By default, the Maven build will fail (or be marked as failed), if the Antora command fails (i.e., exits with a non-zero exit code).
However, if Antora logs an error or warning, the Maven build will not fail by default, meaning the goal is marked as successful.

You can control which log severity level (e.g., error) triggers a failure in Maven by setting Antora's xref:antora:playbook:runtime-log-failure-level.adoc[log failure level].
(The default failure level is `fatal`).
One way to set the failure level is to define it in the Antora playbook.
For example:

[,yml]
----
runtime:
  log:
    failure_level: error
----

Alternately, you can set the `log-failure-level` option in the plugin configuration:

[,xml]
----
<configuration>
  <options>
    <option>log-failure-level error</option>
  </options>
</configuration>
----

You can prevent Antora from failing the build even when Antora encounters a fatal error by setting the failure level to `none`.

[#interactive]
=== Interactive settings

The plugin automatically sets the environment variable `IS_TTY=true` so Antora uses the pretty log format and outputs the location of the published site by default.
You can override this behavior by setting the `IS_TTY` environment variable when running Maven:

 $ IS_TTY=false mvn antora:antora

In this case, the log format will default to json.
You can reverse this change by setting the Antora `log-level` option explicitly:

 $ IS_TTY=false mvn antora:antora "-Dantora.additionalOptions=log-format pretty"

If Maven is running in interactive mode (i.e., not batch mode), the plugin also sets the environment variable `FORCE_COLOR=true` so the pretty log messages are colorized.
You can override this behavior by:

. running Maven in batch mode,

 $ mvn -B antora:antora

. setting the environment variable `NO_COLOR` when running Maven (the value does not matter),

 $ NO_COLOR=true mvn antora:antora

. or setting the `IS_TTY` environment variable as shown above.

These environment variables can also be set in the plugin's xref:antora-goal.adoc#environmentVariables[configuration].

[#skip-install-node-and-npm]
=== Skip Node.js and npm install

If you've run the `antora:antora` goal at least once, you can speed it up by instructing the frontend-maven-plugin to skip the Node.js install and, if applicable, installing npm packages.
To do so, set the `skip.installnodenpm` and `skip.npm` user properties when running Maven.

 $ mvn antora:antora -Dskip.installnodenpm=true -Dskip.npm=true

If you're project does not have a [.path]_package.json_ file with dependencies, the `npx` command is already optimized to skip trying to install packages on subsequent runs.
However, you can reinforce this behavior by forcing `npx` into offline mode.

 $ npm_config_offline=true mvn antora:antora -Dskip.installnodenpm=true

These are both very minor optimizations and you may not need them.
Use with care.

[#node-download-root]
=== Node.js download root

This plugin automatically downloads and installs Node.js and npm on demand (unless the `nodeExecutable` property is specified).
By default, the plugin will download Node.js from the official location at https://nodejs.org.

If your environment mandates downloading Node.js from an alternate URL, you can override the default URL using the `nodeDownloadUrl` parameter or `node.downloadUrl` property.
The download server is expected to mirror the storage structure used by the official download source (e.g., /20.12.2/node-20.12.2-linux-x64.tar.gz).

You can define the `node.downloadUrl` property in the POM file as a custom property:

.pom.xml
[,xml]
----
<properties>
  <node.downloadUrl>https://download.example.org/nodejs</node.downloadUrl>
</properties>
----

or from the CLI using a user property:

 $ mvn antora:antora -Dnode.downloadUrl=https://download.example.org/nodejs

If credentials are required, you can pass a Maven server configuration (defined in settings.xml) using the `nodeDownloadServerId` parameter or `node.downloadServerId` property.

The downloaded file is cached into the local Maven repository ($M2_REPO/com/github/eirslett/node/<nodeVersion>) so it doesn't have to be retrieved again.
Thus, an alternative to using the `node.downloadUrl` property is to use a separate script that seeds the local Maven repository with the downloaded file.
If the plugin finds the file cached in the Maven repository, it will not attempt to reach out to the download server.

=== Escape user properties

When passing user properties to the Maven CLI (i.e., the `-D` option), care must be taken to properly escape the value (and possibly the name).

If the value of the user property contains a space, the entire declaration (i.e., the CLI option) must be enclosed in quotes.

 $ mvn antora:antora "-Dantora.additionalOptions=log-failure-level fatal"

When using PowerShell, this enclosure is necessary if the user property name or value contains a period.
For example:

 $ mvn antora:antora "-Dantora.option.fetch"

It's always safe to enclose the declaration in double quotes regardless of whether it's needed.
